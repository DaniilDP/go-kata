module gitlab.com/DaniilDP/go-kata

go 1.18

require gitlab.com/DaniilDP/greet v0.0.0-20230312185939-806869cad61f

require github.com/brianvoe/gofakeit/v6 v6.20.2

require (
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-sqlite3 v1.14.16
	golang.org/x/sync v0.1.0
)

require (
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
)
