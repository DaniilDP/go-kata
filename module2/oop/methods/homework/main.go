package main

import (
	"fmt"
	"math"
)

type calc struct {
	a, b   float64
	result float64
}

func NewCalc() *calc {
	return &calc{}
}

func (c *calc) SetA(a float64) *calc {
	c.a = a
	return c
}
func (c *calc) SetB(b float64) *calc {
	c.b = b
	return c
}

func (c *calc) Do(operation func(a, b float64) float64) *calc {
	c.result = operation(c.a, c.b)
	return c
}

func (c *calc) Result() float64 {
	return c.result
}

func multiply(a, b float64) float64 {
	return a * b
}

func divide(a, b float64) float64 {
	switch {
	case a == 0 && b == 0:
		return math.NaN()
	case a > 0 && b == 0:
		return math.Inf(1)
	case a < 0 && b == 0:
		return math.Inf(-1)
	}

	return a / b
}

func sum(a, b float64) float64 {
	return a + b
}

func average(a, b float64) float64 {
	return math.Round((a+b)/2*1000000) / 1000000
}

func main() {
	calc := NewCalc()
	res := calc.SetA(10).SetB(20).Do(average).Result()
	fmt.Println(res)
	res2 := calc.Result()
	fmt.Println(res2)
	if res != res2 {
		panic("Calc object state is not persisted")
	}
}
