package main

import (
	"math"
	"testing"
)

func TestMultiply(t *testing.T) {
	var tests = []struct {
		a, b     float64
		expected float64
	}{
		{0, 0, 0},
		{0, 1, 0},
		{1, 0, 0},
		{2, 3, 6},
		{-2, 3, -6},
		{2, -3, -6},
		{-2, -3, 6},
		{0.1, 0.2, 0.02},
	}
	for _, test := range tests {
		actual := multiply(test.a, test.b)
		if math.Round(actual*1e6)/1e6 != math.Round(test.expected*1e6)/1e6 {
			t.Errorf("multiply(%f, %f): expected %f, actual %f", test.a, test.b, test.expected, actual)
		}
	}
}

func TestDivide(t *testing.T) {
	var tests = []struct {
		a, b     float64
		expected float64
	}{
		{0, 1, 0},
		{2, 3, 0.6666666666666666},
		{-2, 3, -0.6666666666666666},
		{2, -3, -0.6666666666666666},
		{-2, -3, 0.6666666666666666},
		{1, 0, math.Inf(1)},
		{-1, 0, math.Inf(-1)},
		{0, 0, math.NaN()},
	}
	for _, test := range tests {
		actual := divide(test.a, test.b)
		if !((actual == test.expected) || (math.IsNaN(actual) && math.IsNaN(test.expected))) {
			t.Errorf("divide(%f, %f): expected %f, actual %f", test.a, test.b, test.expected, actual)
		}
	}
}

func TestSum(t *testing.T) {
	var tests = []struct {
		a, b     float64
		expected float64
	}{
		{0, 0, 0},
		{0, 1, 1},
		{1, 0, 1},
		{2, 3, 5},
		{-2, 3, 1},
		{2, -3, -1},
		{-2, -3, -5},
		{0.1, 0.2, 0.3},
	}
	for _, test := range tests {
		actual := sum(test.a, test.b)
		if math.Round(actual*1e6)/1e6 != math.Round(test.expected*1e6)/1e6 {
			t.Errorf("sum(%f, %f): expected %f, actual %f", test.a, test.b, test.expected, actual)
		}
	}
}

func TestAverage(t *testing.T) {
	var tests = []struct {
		a, b     float64
		expected float64
	}{
		{0, 0, 0},
		{0, 1, 0.5},
		{1, 0, 0.5},
		{2, 3, 2.5},
		{-2, 3, 0.5},
		{2, -3, -0.5},
		{-2, -3, -2.5},
		{0.1, 0.2, 0.15},
	}
	for _, test := range tests {
		actual := average(test.a, test.b)
		if actual != test.expected {
			t.Errorf("average(%f, %f): expected %f, actual %f", test.a, test.b, test.expected, actual)
		}
	}
}
