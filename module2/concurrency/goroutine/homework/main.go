package main

import (
	"fmt"
	"time"
)

//func main() {
//	t := time.Now()
//	rand.Seed(t.UnixNano())
//
//	go parseURL("https://www.youtube.com/")
//	parseURL("https://www.example.com/")
//
//	fmt.Printf("Parsing completed. Time Elapsed: %.2f seconds\n", time.Since(t).Seconds())
//}
//
//func parseURL(url string) {
//	for i := 0; i < 5; i++ {
//		latency := rand.Intn(500) + 500
//
//		time.Sleep(time.Duration(latency) * time.Millisecond)
//
//		fmt.Printf("Parsing <%s> - Step %d - Latency %d ms\n", url, i+1, latency)
//	}
//}

func main() {
	message1 := make(chan string)
	message2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			message1 <- "прошло пол секунды"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			message2 <- "прошло 2 секунды"
		}
	}()

	for {
		select {
		case msg := <-message1:
			fmt.Println(msg)
		case msg := <-message2:
			fmt.Println(msg)
		}
	}

}
