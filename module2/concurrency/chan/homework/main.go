package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func isClosed(ch <-chan int) bool {
    select {
    case <-ch:
        return true
    default:
    }

    return false
}


func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}
		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}
//func generateData(stopCh <-chan struct{}) chan int {
func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			// case <-stopCh:
			// 	for len(out) < cap(out) {
			// 		<-out
			// 	}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	start := time.Now()
	rand.Seed(time.Now().UnixNano())
	
	
	
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	//stopCh := make(chan struct{})
	out := generateData()//stopCh

	go func() {
		for num := range out {
			a <- num
		}
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		close(c)
	}()

	mainChan := joinChannels(a, b, c)

	ticker := time.NewTicker(30 * time.Second)
	defer ticker.Stop()

	for {
		select {
		case num, ok := <-mainChan:
			if !ok {
				fmt.Println("All channels closed")
				//close(stopCh)
				return
			}
			fmt.Println(num)
		case <-ticker.C:
			elapsed := time.Since(start).Seconds()
			fmt.Println("Тайм Аут!")
			fmt.Printf("A: %t \nB: %t\nC: %t\nOUT: %t\nMainChan: %t\n", isClosed(a), isClosed(b), isClosed(c), isClosed(out), isClosed(mainChan))
			fmt.Printf("Программа выполнялась %.2f секунд\n", elapsed)
			return
		}
	}
}
/*
Тайм Аут!
A: true
B: true
C: true
OUT: true
MainChan: true
Программа выполнялась 30.02 секунд
*/
