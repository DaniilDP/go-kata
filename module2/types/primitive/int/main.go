package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")
	var uint8Number uint8 = 1 << 7
	var min8 = int8(uint8Number)
	uint8Number--
	var max8 = int8(uint8Number)
	fmt.Println("int8 min value:", min8, "int8 max value:", max8, "size", unsafe.Sizeof(min8), "bytes")
	var uint16Number uint16 = 1 << 15
	var min16 = int16(uint16Number)
	uint16Number--
	var max16 = int16(uint16Number)
	fmt.Println("int16 min value:", min16, "int16 max value:", max16, "size", unsafe.Sizeof(min16), "bytes")
	var uint32Number uint32 = 1 << 31
	var min32 = int32(uint32Number)
	uint32Number--
	var max32 = int32(uint32Number)
	fmt.Println("int32 min value:", min32, "int32 max value:", max32, "size", unsafe.Sizeof(min32), "bytes")
	var uint64Number uint64 = 1 << 63
	var min64 = int64(uint64Number)
	uint64Number--
	var max64 = int64(uint64Number)
	fmt.Println("int64 min value:", min64, "int64 max value:", max64, "size", unsafe.Sizeof(min64), "bytes")

	fmt.Println("=== END type int ===")
}
