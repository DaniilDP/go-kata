package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch v := r.(type) {
	case nil:
		fmt.Println("Success! r is nil")
	case *int:
		if v == nil {
			fmt.Println("Success!")
		}
	default:
		fmt.Printf("Unexpected type %T\n", v)
	}
}
