package main

import "fmt"

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	//убрал повторное определение и линтер сказал объеденить
	var i Userer = &User{}
	_ = i
	fmt.Println("Success!")
}
