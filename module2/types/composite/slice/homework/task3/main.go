package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}

	fmt.Println("Оригинал:      ", users)

	//удаление первого элемента
	users_cut_first := users[1:]
	fmt.Println("Без первого:              ", users_cut_first)

	//удаление последнего элемента
	users_cut_last := users[:len(users)-1]
	fmt.Println("Без последнего:", users_cut_last)

}
