package main

import "fmt"

func main() {
	//Первый способ
	s := []int{1, 2, 3}
	s = Append(s)
	fmt.Println("первый способ: ", s)

	//Второй способ
	s2 := []int{1, 2, 3}
	Append2(&s2)
	fmt.Println("второй способ:", s2)
}

func Append(s []int) []int {
	s = append(s, 4)
	return s
}

func Append2(s *[]int) {
	*s = append(*s, 4)
}
