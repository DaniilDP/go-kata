package main

import (
	"fmt"
)

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/kubernetes/kubernetes",
			Stars: 96200,
		},
		{
			Name:  "https://github.com/apache/spark",
			Stars: 35100,
		},
		{
			Name:  "https://github.com/Microsoft/vscode",
			Stars: 143000,
		},
		{
			Name:  "https://github.com/NixOS/nixpkgs",
			Stars: 11500,
		},
		{
			Name:  "https://github.com/rust-lang/rust",
			Stars: 78300,
		},
		{
			Name:  "https://github.com/dotnet/roslyn",
			Stars: 17100,
		},
		{
			Name:  "https://github.com/tensorflow/tensorflow",
			Stars: 172000,
		},
		{
			Name:  "https://github.com/dotnet/corefx",
			Stars: 17800,
		},
		{
			Name:  "https://github.com/cockroachdb/cockroach",
			Stars: 26700,
		},
		{
			Name:  "https://github.com/joomla/joomla-cms",
			Stars: 4400,
		},
		{
			Name:  "https://github.com/pandas-dev/pandas",
			Stars: 37000,
		},
	}

	projectMap := make(map[string]Project)

	for _, project := range projects {
		projectMap[project.Name] = project
	}

	for name, project := range projectMap {
		fmt.Printf("%s - %d stars\n", name, project.Stars)
	}
}
