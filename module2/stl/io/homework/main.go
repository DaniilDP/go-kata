package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	// создаем буфер
	buffer := bytes.Buffer{}

	// записываем данные в буфер
	for _, d := range data {
		_, err := buffer.WriteString(d + "\n")
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	// создаем файл
	file, err := os.Create("example.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	// записываем данные в файл, используя io.Copy
	_, err = io.Copy(file, &buffer)
	if err != nil {
		fmt.Println(err)
		return
	}

	// создаем новый буфер
	newBuffer := bytes.Buffer{}

	//указатель в начало файла
	_, _ = file.Seek(0, io.SeekStart)

	// читаем данные из файла и записываем в новый буфер
	_, err = io.Copy(&newBuffer, file)
	if err != nil {
		fmt.Println(err)
		return
	}

	// выводим данные из буфера

	fmt.Println(newBuffer.String())
}
