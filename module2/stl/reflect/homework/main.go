package main

import (
	"fmt"
	"reflect"
	"time"
)

var reflectedStructs map[string][]Field

type Field struct {
	Name string
	Tags map[string]string
}

type UserDTO struct {
	ID            int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name          string    `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	Phone         string    `json:"phone" db:"phone" db_type:"varchar(34)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Email         string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Password      string    `json:"password" db:"password" db_type:"varchar(144)" db_default:"default null" db_ops:"create,update"`
	Status        int       `json:"status" db:"status" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	Role          int       `json:"role" db:"role" db_type:"int" db_default:"not null" db_ops:"create,update"`
	Verified      bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	EmailVerified bool      `json:"email_verified" db:"email_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	PhoneVerified bool      `json:"phone_verified" db:"phone_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt     time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt     time.Time `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt     time.Time `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

type EmailVerifyDTO struct {
	ID        int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Email     string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"not null" db_index:"index,unique" db_ops:"create,update"`
	UserID    int       `json:"user_id,omitempty" db:"user_id" db_ops:"create" db_type:"int" db_default:"not null" db_index:"index"`
	Hash      string    `json:"hash,omitempty" db:"hash" db_ops:"create" db_type:"char(36)" db_default:"not null" db_index:"index"`
	Verified  bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
}

func main() {
	structs := []interface{}{
		UserDTO{},
		EmailVerifyDTO{},
	}

	reflectedStructs = make(map[string][]Field, len(structs))

	for _, s := range structs {
		structName := reflect.TypeOf(s).Name()
		fields := []Field{}
		for i := 0; i < reflect.TypeOf(s).NumField(); i++ {
			field := reflect.TypeOf(s).Field(i)

			tags := make(map[string]string)

			for _, tag := range []string{"json", "db", "db_type", "db_default", "db_index", "db_ops"} {
				if val, ok := field.Tag.Lookup(tag); ok {
					tags[tag] = val
				}
			}

			fields = append(fields, Field{
				Name: field.Name,
				Tags: tags,
			})
		}

		reflectedStructs[structName] = fields
	}
	

	fmt.Println(reflectedStructs)
}

/*
map[
	EmailVerifyDTO:[
		{ID map[db:id db_default:not null db_type:BIGSERIAL primary key json:id]} 
		{Email map[db:email db_default:not null db_index:index,unique db_ops:create,update db_type:varchar(89) json:email]} 
		{UserID map[db:user_id db_default:not null db_index:index db_ops:create db_type:int json:user_id,omitempty]} 
		{Hash map[db:hash db_default:not null db_index:index db_ops:create db_type:char(36) json:hash,omitempty]} 
		{Verified map[db:verified db_default:not null db_ops:create,update db_type:boolean json:verified]} 
		{CreatedAt map[db:created_at db_default:default (now()) not null db_index:index db_type:timestamp json:created_at]}] 
	UserDTO:[{ID map[db:id db_default:not null db_type:BIGSERIAL primary key json:id]} 
			{Name map[db:name db_default:default null db_ops:create,update db_type:varchar(55) json:name]} 
			{Phone map[db:phone db_default:default null db_index:index,unique db_ops:create,update db_type:varchar(34) json:phone]} 
			{Email map[db:email db_default:default null db_index:index,unique db_ops:create,update db_type:varchar(89) json:email]} 
			{Password map[db:password db_default:default null db_ops:create,update db_type:varchar(144) json:password]} 
			{Status map[db:status db_default:default 0 db_ops:create,update db_type:int json:status]} 
			{Role map[db:role db_default:not null db_ops:create,update db_type:int json:role]} 
			{Verified map[db:verified db_default:not null db_ops:create,update db_type:boolean json:verified]} 
			{EmailVerified map[db:email_verified db_default:not null db_ops:create,update db_type:boolean json:email_verified]} 
			{PhoneVerified map[db:phone_verified db_default:not null db_ops:create,update db_type:boolean json:phone_verified]} 
			{CreatedAt map[db:created_at db_default:default (now()) not null db_index:index db_type:timestamp json:created_at]} 
			{UpdatedAt map[db:updated_at db_default:default (now()) not null db_index:index db_type:timestamp json:updated_at]} 
			{DeletedAt map[db:deleted_at db_default:default null db_index:index db_type:timestamp json:deleted_at]}]]
*/