package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

type Person struct {
	Name  string
	Age   int
	Money float64
}

func generateSelfStory(p Person) string {
	return fmt.Sprintf(
		"I am %s, %d years old, and work as a %s. In my free time, I enjoy %s. I live in %s, with my pet %s who is %s. I love to eat %s and drink %s. I currently have %.2f dollars in my bank account. I'm feeling great about life!",
		p.Name,
		p.Age,
		gofakeit.JobTitle(),
		gofakeit.Hobby(),
		gofakeit.Country(),
		gofakeit.Animal(),
		gofakeit.Color(),
		gofakeit.MinecraftFood(),
		gofakeit.BeerName(),
		p.Money,
	)
}

func main() {
	rand.Seed(time.Now().UnixNano())

	for i := 0; i < 100; i++ {
		p := Person{
			Name:  gofakeit.Name(),
			Age:   rand.Intn(50) + 18,
			Money: float64(rand.Intn(50000) + 1),
		}
		story := generateSelfStory(p)
		fmt.Println(story)
	}
}
