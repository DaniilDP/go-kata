package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
)

type Config struct {
	AppName    string `json:"AppName"`
	Production bool   `json:"Production"`
}

func main() {
	var ConfPath string
	flag.StringVar(&ConfPath, "conf", "", "file path to json")
	flag.Parse()

	var in io.Reader
	if ConfPath != "" {
		f, err := os.Open(ConfPath)
		if err != nil {
			fmt.Println("error opening file: err:", err)
			os.Exit(1)
		}
		defer f.Close()

		in = f
	} else {
		in = os.Stdin
	}

	buf := new(bytes.Buffer)
	if _, err := io.Copy(buf, in); err != nil {
		fmt.Fprintln(os.Stderr, "error reading: err:", err)
	}
	conf := Config{}
	_ = json.Unmarshal(buf.Bytes(), &conf)

	formatted, _ := json.MarshalIndent(conf, "", "  ")
	fmt.Println(string(formatted))
}
