package main

import (
	"bufio"
	"fmt"
	"os"
	"unicode/utf8"
)

var rules = map[rune]string{
	'А': "A", 'а': "a", 'Б': "B", 'б': "b", 'В': "V", 'в': "v", 'Г': "G", 'г': "g",
	'Д': "D", 'д': "d", 'Е': "E", 'е': "e", 'Ё': "Yo", 'ё': "yo", 'Ж': "Zh", 'ж': "zh",
	'З': "Z", 'з': "z", 'И': "I", 'и': "i", 'Й': "Y", 'й': "y", 'К': "K", 'к': "k",
	'Л': "L", 'л': "l", 'М': "M", 'м': "m", 'Н': "N", 'н': "n", 'О': "O", 'о': "o",
	'П': "P", 'п': "p", 'Р': "R", 'р': "r", 'С': "S", 'с': "s", 'Т': "T", 'т': "t",
	'У': "U", 'у': "u", 'Ф': "F", 'ф': "f", 'Х': "Kh", 'х': "kh", 'Ц': "Ts", 'ц': "ts",
	'Ч': "Ch", 'ч': "ch", 'Ш': "Sh", 'ш': "sh", 'Щ': "Sch", 'щ': "sch", 'Ъ': "", 'ъ': "",
	'Ы': "Y", 'ы': "y", 'Ь': "", 'ь': "", 'Э': "E", 'э': "e", 'Ю': "Yu", 'ю': "yu",
	'Я': "Ya", 'я': "ya",
}

func Ttranslit(data string) string {
	var result string
	for len(data) > 0 {
		r, size := utf8.DecodeRuneInString(data)
		if rule, ok := rules[r]; ok {
			result += rule
		} else {
			result += string(r)
		}
		data = data[size:]
	}
	return result
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	inputFile := "example.txt"
	file, err := os.Open(inputFile)
	check(err)

	defer file.Close()
	output, err := os.Create("example.processed.txt")
	check(err)
	defer output.Close()

	scanner := bufio.NewScanner(file)
	writer := bufio.NewWriter(output)

	for scanner.Scan() {
		line := scanner.Text()
		transLine := Ttranslit(line)
		if _, err := writer.WriteString(transLine + "\n"); err != nil {
			fmt.Println("Error writing to output file:", err)
			return
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading input file:", err)
		return
	}

	writer.Flush()
	fmt.Println("Conversion complete!")
}
