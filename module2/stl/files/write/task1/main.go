package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	// Базара зиро
	// Напиши программу, записывающую данные приходящие с os.Stdin в ->
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter your name: ")
	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)

	file, err := os.OpenFile("main.go", os.O_APPEND|os.O_WRONLY, 0644)
	check(err)
	defer file.Close()

	_, err = file.WriteString("\n" + name)
	check(err)
}
