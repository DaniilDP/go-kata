package main

import "testing"

var (
	users    = GenUsers()
	products = GenProducts()
)

func BenchmarkMapUserProducts(b *testing.B) {
	for n := 0; n < b.N; n++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkMapUserProducts2(b *testing.B) {
	for n := 0; n < b.N; n++ {
		MapUserProducts2(users, products)
	}
}

/*
BenchmarkMapUserProducts-8          1000             88160 ns/op
BenchmarkMapUserProducts2-8         1000             52736 ns/op
*/
