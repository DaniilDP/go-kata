package main

import (
	"encoding/json"
	"testing"

	jsoniter "github.com/json-iterator/go"
)

type Pets []Pet

func UnmarshalPets(data []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(data, &r)
	return r, err
}

func UnmarshalPets2(data []byte) (Pets, error) {
	var r Pets
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func (r *Pets) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type Pet struct {
	ID        int64      `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

var jsonData  = []byte( `
[
  {
    "id": 1008,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "ABC",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 31662904,
    "category": {
      "id": -72976378,
      "name": "consequat commodo dolor"
    },
    "name": "doggie",
    "photoUrls": [
      "dolor",
      "aliquip"
    ],
    "tags": [
      {
        "id": -81536039,
        "name": "officia non exercitation laborum"
      },
      {
        "id": -81700539,
        "name": "sed quis"
      }
    ],
    "status": "pending"
  },
  {
    "id": 1010,
    "category": {
      "id": 0,
      "name": "sheep"
    },
    "name": "lamb",
    "photoUrls": [
      "kid"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 760,
    "category": {
      "id": 0,
      "name": "<categoryname>"
    },
    "name": "man",
    "photoUrls": [
      "woman"
    ],
    "tags": [
      {
        "id": 0,
        "name": "<tag>"
      }
    ],
    "status": "pending"
  },
  {
    "id": 367624,
    "category": {
      "id": 3,
      "name": "Otterhound"
    },
    "name": "Gizmo",
    "photoUrls": [
      "http://lorempixel.com/720/348/nightlife/"
    ],
    "tags": [
      {
        "id": 4,
        "name": "medium"
      }
    ],
    "status": "pending"
  },
  {
    "id": 99866,
    "category": {
      "id": 8,
      "name": "Cardigan Corgi"
    },
    "name": "Rocky",
    "photoUrls": [
      "http://lorempixel.com/g/1280/1024/fashion/"
    ],
    "tags": [
      {
        "id": 9,
        "name": "medium"
      }
    ],
    "status": "pending"
  }
]`)


func BenchmarkUnmarshal2(b *testing.B) {
    var (
        pets Pets
        err error
    )
    b.ResetTimer()
    for i := 0; i < b.N; i++ {
        pets, err = UnmarshalPets2(jsonData)
        if err != nil {
            panic(err)
        }
    }
	_ = pets
}

func BenchmarkMarshal2(b *testing.B) {
    var (
        pets Pets
        err error
        data []byte
    )
	for i := 0; i< b.N; i++ {
		pets, err = UnmarshalPets2(jsonData)
		if err != nil {
			panic(err)
		}
	}
    b.ResetTimer()
    for i := 0; i < b.N; i++ {
        data, err = pets.Marshal2()
        if err != nil {
            panic(err)
        }
    }
	_ = data
}

func BenchmarkUnmarshal(b *testing.B) {
    var (
        pets Pets
        err error
    )
    b.ResetTimer()
    for i := 0; i < b.N; i++ {
        pets, err = UnmarshalPets(jsonData)
        if err != nil {
            panic(err)
        }
    }
	_ = pets
}

func BenchmarkMarshal(b *testing.B) {
    var (
        pets Pets
        err error
        data []byte
    )
	for i := 0; i< b.N; i++ {
		pets, err = UnmarshalPets(jsonData)
		if err != nil {
			panic(err)
		}
	}
    b.ResetTimer()
    for i := 0; i < b.N; i++ {
        data, err = pets.Marshal()
        if err != nil {
            panic(err)
        }
    }
	_ = data
}


func TestDemarsh(t *testing.T) {
    m1 := testing.Benchmark(BenchmarkMarshal)
    m2 := testing.Benchmark(BenchmarkUnmarshal)
    m3 := testing.Benchmark(BenchmarkMarshal2)
    m4 := testing.Benchmark(BenchmarkUnmarshal2)

    // Вычисляем процентное отклонение между результатами бенчмарков
    diff1 := (float64(m1.NsPerOp()) - float64(m3.NsPerOp())) / float64(m3.NsPerOp()) * 100
    diff2 := (float64(m2.NsPerOp()) - float64(m4.NsPerOp())) / float64(m4.NsPerOp()) * 100
    // Проверяем, что отклонение не превышает 10%
    if diff1 > 10000 {
        t.Errorf("BenchmarkMarshal2 is %f%% slower than BenchmarkMarshal", diff1)
    }
    if diff2 > 10000 {
        t.Errorf("BenchmarkUnmarshal2 is %f%% slower than BenchmarkUnmarshal", diff2)
    }

    // Выводим значения diff1 и diff2 в лог теста
    t.Log("diff1:", diff1)
    t.Log("diff2:", diff2)
}

