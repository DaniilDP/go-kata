package main

import (
	"fmt"
)

func main() {
	fmt.Println("i can manage")
	go func() {
		// runtime.Gosched() // / раскоментировать для передачи управления основной горутине ("goroutines in Golang!" не будет напечатанно )
		fmt.Println("goroutines in Golang!")
	}()
	// runtime.Gosched() // раскоментировать для передачи управления 2 горутине (будет напечатанно "goroutines in Golang!")
	fmt.Println("and its awesome!")
}
