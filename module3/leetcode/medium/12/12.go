package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	_ = mergeInBetween(&ListNode{}, 0, 0, &ListNode{})
}

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	var idx int
	curr := list1
	var prev *ListNode
	for curr != nil && curr.Next != nil {
		if idx == (a - 1) {
			prev = curr.Next
			curr.Next = list2
			for curr != nil && curr.Next != nil {
				curr = curr.Next
			}
			break
		}
		idx++
		curr = curr.Next
	}

	for idx < b {
		prev = prev.Next
		idx++
	}

	curr.Next = prev

	return list1
}
