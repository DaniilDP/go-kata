package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	_ = findSmallestSetOfVertices(0, nil)
}

func findSmallestSetOfVertices(n int, edges [][]int) []int {

	degrees := make([]int, n)
	out := []int{}

	for _, v := range edges {
		degrees[v[1]]++
	}

	for i, v := range degrees {
		if v == 0 {
			out = append(out, i)
		}
	}
	return out
}
