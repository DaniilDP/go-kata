package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	_ = xorQueries([]int{}, nil)
}

func xorQueries(arr []int, queries [][]int) []int {
	prefix := make([]int, len(arr)+1)
	prefix[0] = 0
	for i := 1; i < len(arr)+1; i++ {
		prefix[i] = prefix[i-1] ^ arr[i-1]
	}

	res := make([]int, len(queries))
	for i, q := range queries {
		res[i] = prefix[q[0]] ^ prefix[q[1]+1]
	}

	return res
}
