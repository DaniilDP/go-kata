package main

import (
	"errors"
	"fmt"
)

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
} 

func main() {
	root := &TreeNode{Val: 4,}
	for _, v := range []int{1,6,2,5,7,3,8} {
		_ = root.Insert(v)
	}
	printTree(bstToGst(root), 0)
	
}

func bstToGst(root *TreeNode) *TreeNode {
    var lst []*TreeNode
    
    var dfs func(*TreeNode)
    dfs = func(root *TreeNode) {
        if root == nil {
            return 
        }
        
        dfs(root.Right)
        lst = append(lst, root)
        dfs(root.Left)
    }
    
    dfs(root)
    
    prefixSum := 0
    
    for _, v := range lst {
        oldValue := v.Val
        v.Val += prefixSum
        prefixSum += oldValue
    }
    
    return root
}

func (t *TreeNode) Insert(value int) error {

	if t == nil {
		return errors.New("Tree is nil")
	}
	if t.Val == value {
		return errors.New("This node value already exists")
	}
	if t.Val > value {
		if t.Left == nil {
			t.Left = &TreeNode{Val: value}
			return nil
		}
		return t.Left.Insert(value)
	}
	if t.Val < value {
		if t.Right == nil {
			t.Right = &TreeNode{Val: value}
			return nil
		}
		return t.Right.Insert(value)
	}
	return nil
}

func printTree(node *TreeNode, level int) {
	if node == nil {
		return
	}
	printTree(node.Right, level+1)
	for i := 0; i < level; i++ {
		fmt.Print("   ")
	}
	fmt.Println(node.Val)
	printTree(node.Left, level+1)
}
