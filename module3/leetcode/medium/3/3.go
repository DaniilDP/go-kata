package main

import (
	"fmt"
)

 type ListNode struct {
     Val int
     Next *ListNode
 }

func main() {
	list := &ListNode{Val: 0,}
	head := list
	for _, v := range []int{3,1,0,4,5,2,0}{
		list = AddListNode(v, list)
	}
	fmt.Println(mergeNodes(head).Next.Val) 
}

func AddListNode(val int, head *ListNode ) *ListNode {
	head.Next = &ListNode{Val: val}
	return head.Next
}

func mergeNodes(head *ListNode) *ListNode {
	result := &ListNode{}
	headresult := result
	summ := 0
	for head != nil{
		if head.Val == 0 && summ != 0 {
			if result.Val == 0 {
				result.Val = summ
			} else {
				result.Next = &ListNode{Val: summ}
				result = result.Next
			}
			summ = 0	
		}
		summ += head.Val
		head = head.Next
	}
	return headresult
}
