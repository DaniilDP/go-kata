package main

import "fmt"

func main() {
	fmt.Println(sortTheStudents([][]int{{10, 6, 9, 1}, {7, 5, 11, 2}, {4, 8, 3, 15}}, 2)) //[[7,5,11,2],[10,6,9,1],[4,8,3,15]]
}

func sortTheStudents(score [][]int, k int) [][]int {
bubble:
	for i, v := range score {
		if i+1 >= len(score) {
			break
		}
		if v[k] < score[i+1][k] {
			score[i], score[i+1] = score[i+1], score[i]
			goto bubble
		}
	}
	return score
}
