package main

import (
	"errors"
	"fmt"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	root := &TreeNode{Val: 1}
	for _, v := range []int{2, 3, 3, 4} {
		_ = root.Insert(v)
	}
	printTree(balanceBST(root), 0)
}

func balanceBST(root *TreeNode) *TreeNode {
	var arr []int
	inOrder(root, &arr)
	return buildTree(arr, 0, len(arr)-1)
}

func inOrder(node *TreeNode, arr *[]int) {
	if node == nil {
		return
	}
	inOrder(node.Left, arr)
	*arr = append(*arr, node.Val)
	inOrder(node.Right, arr)
}

func buildTree(arr []int, start int, end int) *TreeNode {
	if start > end {
		return nil
	}
	mid := (start + end) / 2
	node := &TreeNode{Val: arr[mid]}
	node.Left = buildTree(arr, start, mid-1)
	node.Right = buildTree(arr, mid+1, end)
	return node
}

func (t *TreeNode) Insert(value int) error {

	if t == nil {
		return errors.New("Tree is nil")
	}
	if t.Val == value {
		return errors.New("This node value already exists")
	}
	if t.Val > value {
		if t.Left == nil {
			t.Left = &TreeNode{Val: value}
			return nil
		}
		return t.Left.Insert(value)
	}
	if t.Val < value {
		if t.Right == nil {
			t.Right = &TreeNode{Val: value}
			return nil
		}
		return t.Right.Insert(value)
	}
	return nil
}

func printTree(node *TreeNode, level int) {
	if node == nil {
		return
	}
	printTree(node.Right, level+1)
	for i := 0; i < level; i++ {
		fmt.Print("   ")
	}
	fmt.Println(node.Val)
	printTree(node.Left, level+1)
}
