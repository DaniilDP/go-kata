package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	_ = deepestLeavesSum(&TreeNode{})
}

func deepestLeavesSum(root *TreeNode) int {
	if root == nil {
		return 0
	}

	queue := []*TreeNode{root}
	sum := 0
	for len(queue) > 0 {

		nodesIncurrentHeight := len(queue)
		sum = 0
		for i := 0; i < nodesIncurrentHeight; i++ {
			curr := queue[0]
			queue = queue[1:]
			sum += curr.Val
			if curr.Left != nil {
				queue = append(queue, curr.Left)

			}

			if curr.Right != nil {
				queue = append(queue, curr.Right)
			}
		}

	}
	return sum
}

//func printTree(node *TreeNode, level int) {
//	if node == nil {
//		return
//	}
//	printTree(node.Right, level+1)
//	for i := 0; i < level; i++ {
//		fmt.Print("   ")
//	}
//	fmt.Println(node.Val)
//	printTree(node.Left, level+1)
//}
