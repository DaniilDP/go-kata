package main

import (
	"fmt"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	nums := []int{3, 2, 1, 6, 0, 5}
	printTree(constructMaximumBinaryTree(nums), 0)
}

func constructMaximumBinaryTree(nums []int) *TreeNode {
	var insertmaxvalue func(*TreeNode, []int)
	insertmaxvalue = func(root *TreeNode, num []int) {
		if len(num) > 0 {
			maxi := MaxIndex(num)
			root.Val = num[maxi]
			if len(num[:maxi]) > 0 {
				root.Left = &TreeNode{}
				insertmaxvalue(root.Left, num[:maxi])
			}
			if len(num[maxi+1:]) > 0 {
				root.Right = &TreeNode{}
				insertmaxvalue(root.Right, num[maxi+1:])
			}
		}
	}
	root := &TreeNode{}
	insertmaxvalue(root, nums)
	return root
}

func MaxIndex(list []int) int {
	maxIndex := 0
	for i := 1; i < len(list); i++ {
		if list[i] > list[maxIndex] {
			maxIndex = i
		}
	}
	return maxIndex
}

func printTree(node *TreeNode, level int) {
	if node == nil {
		return
	}
	printTree(node.Right, level+1)
	for i := 0; i < level; i++ {
		fmt.Print("   ")
	}
	fmt.Println(node.Val)
	printTree(node.Left, level+1)
}
