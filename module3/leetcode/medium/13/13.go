package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	_ = maxSum(nil)
}

func maxSum(grid [][]int) int {
	max := 0
	m, n := len(grid), len(grid[0])

	sum := func(i, j int) int {
		s := grid[i][j]
		for x := j - 1; x <= j+1; x++ {
			s += grid[i-1][x]
			s += grid[i+1][x]
		}
		return s
	}

	for i := 1; i < m-1; i++ {
		for j := 1; j < n-1; j++ {
			s := sum(i, j)
			if s > max {
				max = s
			}
		}
	}
	return max
}
