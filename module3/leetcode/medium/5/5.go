package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	head := &ListNode{Val: 4}
	foot := head
	for _, v := range []int{2,2,3} {
		foot = AddListNode(v, foot)
	}
	fmt.Println(pairSum(head)) // 182
}

func pairSum(head *ListNode) int {
	result := 0
	var lst []*ListNode

	for head != nil {
		lst = append(lst, head)
		head = head.Next
	}

	j := len(lst)-1

	for _, v := range lst[:len(lst)/2] {
		if result < v.Val + lst[j].Val {
			result = v.Val + lst[j].Val
		}
		j--
	}
	return result
}

func AddListNode(val int, head *ListNode ) *ListNode {
	head.Next = &ListNode{Val: val}
	return head.Next
}
