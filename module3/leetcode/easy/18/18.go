package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println(differenceOfSum([]int{1, 15, 6, 3})) // 0
}

func differenceOfSum(nums []int) int {
	summE, summC := 0, 0
	for _, i := range nums {
		summE += i
		summC += i
		if i > 9 {
			summC -= i
			str := strconv.Itoa(i)
			for _, j := range str {
				s, _ := strconv.Atoi(string(j))
				summC += s
			}
		}
	}

	summ := summE - summC

	if summ < 0 {
		return -summ
	}
	return summ

}