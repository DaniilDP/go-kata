package main

import (
	"fmt"
)

func main() {
	fmt.Println(numIdenticalPairs([]int{1,2,3,1,1,3})) // 4
}

func numIdenticalPairs(nums []int) int {
	count := 0
	for i, v := range nums {
		for j := i+1; j < len(nums); j++{
			if v == nums[j] {
				count++
			}
		}
	}
	return count
}