package main

import (
	"fmt"
)

func main() {
	fmt.Println(runningSum([]int{1,1,1,1,1})) // [1,2,3,4,5]
}

func runningSum(nums []int) []int {
	for i, v := range nums {
		if i == 0 {
			continue
		} else {
			nums[i] = v + nums[i-1]
		}
	}
	return nums
}