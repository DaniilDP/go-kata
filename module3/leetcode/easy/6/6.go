package main

import "fmt"

func main() {
	fmt.Println(uniqueMorseRepresentations([]string{"gin","zen","gig","msg"}))
}

func uniqueMorseRepresentations(words []string) int {

	ABCM := []string{".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."}
	ABC := []string{"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"}
	mapword := make(map[string]int, len(words))
	for _, word := range words {
		resword := ""
		for _, letter := range word {
			resword += ABCM[search(string(letter), ABC)]
		}
		mapword[resword] += 1
	}
	return len(mapword)
}

func search(key string, arr []string) int {
    
    for index, value := range arr {
       if value == key {
          return index
       }
    }
    
    return -1
 }