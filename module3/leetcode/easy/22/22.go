package main

import (
	"fmt"
)

func main() {
	fmt.Println(smallerNumbersThanCurrent([]int{6,5,4,8})) // [2,1,0,3]
}

func smallerNumbersThanCurrent(nums []int) []int {
	result := make([]int, len(nums))
	for idi, i := range nums {
		for idj, j := range nums {
			if idj != idi && i > j {
				result[idi] += 1
			}
		}
	}
	return result
}