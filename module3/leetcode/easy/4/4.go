package main

import "fmt"

func main() {
	fmt.Println(buildArray([]int{5,0,1,2,3,4})) //[4,5,0,1,2,3]
}

func buildArray(nums []int) []int {
	var ans []int 
	for _, v := range nums{
		ans = append(ans, nums[v])
	}
	return ans
}