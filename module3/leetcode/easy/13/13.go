package main

import (
	"fmt"
)

func main() {
	fmt.Println(numJewelsInStones("aA", "aAAbbbb")) // 3
}

func numJewelsInStones(jewels string, stones string) int {
	count := 0
	for _, i := range jewels {
		for _, j := range stones {
			if i == j {
				count++
			}
		}
	}
	return count
}