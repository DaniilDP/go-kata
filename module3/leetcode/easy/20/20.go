package main

import (
	"fmt"
)

func main() {
	fmt.Println(kidsWithCandies([]int{4,2,1,1,2}, 1)) // [true,false,false,false,false]
}

func kidsWithCandies(candies []int, extraCandies int) []bool {
    result := make([]bool, len(candies))
    maxCandies := 0
    for _, num := range candies{
        if num > maxCandies {
            maxCandies = num
        }
    }

    for idx, numOfCandies := range candies{
        if (numOfCandies + extraCandies) >= maxCandies  {
            result[idx] = true
        } else {
            result[idx] = false
        }
    }
    return result
}