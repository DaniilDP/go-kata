package main

import (
	"fmt"
	"sort"
	"strconv"
)

func main() {
	fmt.Println(minimumSum(4009)) // 52
}

func minimumSum(num int) int {
	digits := []int{(((num / 100) / 10)), (((num / 100) % 10)), (((num % 100) / 10)), (((num % 100) % 10))}
	sort.Ints(digits)
	new1, _ := strconv.Atoi(strconv.Itoa(digits[0])+strconv.Itoa(digits[2]))
	new2, _ := strconv.Atoi(strconv.Itoa(digits[1])+strconv.Itoa(digits[3]))
	return new1 + new2
}
