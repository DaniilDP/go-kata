package main

import (
	"fmt"
)

func main() {
	fmt.Println(finalValueAfterOperations([]string{"++X","++X","X++"})) // 3 // на leetcode нет проблов между X и ++ 
}

func finalValueAfterOperations(operations []string) int {
	x := 0
	for _, operation := range operations{
		if operation == "X++" || operation == "++X"{
			x++
		} else {
			x--
		}
	}
	return x

}

