package main

import (
	"fmt"
)

func main() {
	fmt.Println(findKthPositive([]int{1,2}, 1)) // 6
}

func findKthPositive(arr []int, k int) int {
	i := 1
    for k != 0 {
		if !search(i, arr) {
			k--
		}
		i++
	}
	return i-1
}

func search(key int, arr []int) bool {
    for _, value := range arr {
       if value == key {
          return true
       }
    }
    return false
 }