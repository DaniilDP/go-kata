package main

import (
	"fmt"
)

func main() {
	fmt.Println(smallestEvenMultiple(5)) // 10
}

func smallestEvenMultiple(n int) int {
	switch n%2 {
	case 1:
		return n*2
	case 0:
		return n
	}
	return  0
}