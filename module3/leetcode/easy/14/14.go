package main

import (
	"fmt"
)

func main() {
	fmt.Println(maximumWealth([][]int{{1,5}, {7,3}, {3,5}})) // 3
}

func maximumWealth(accounts [][]int) int {
	result := 0
	for _, i := range accounts {
		sum := 0
		for _, j := range i {
			sum += j
		}
		if sum > result {
			result = sum
		}
	}
	return result
}