package main

import (
	"fmt"
)

func main() {
	fmt.Println(shuffle([]int{1,2,3,4,4,3,2,1}, 4)) // 1,4,2,3,3,2,4,1
}

func shuffle(nums []int, n int) []int {
	leftpart := nums[:n]
	rightpart := nums[n:]
	result := []int{}
	for i, v := range leftpart {
		result = append(result, v)
		result = append(result, rightpart[i])
	}
	return result
}