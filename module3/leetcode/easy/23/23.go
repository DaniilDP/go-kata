package main

import (
	"fmt"

	"strings"
)

func main() {
	fmt.Println(interpret("G()()()()(al)")) //  "Gooooal"
}

func interpret(command string) string {
	command = strings.Replace(command, "()", "o", -1)
	command = strings.Replace(command, "(al)", "al", -1)
	return command
}