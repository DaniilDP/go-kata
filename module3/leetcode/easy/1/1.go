package main

func main() {
	println(tribonacci(37))
}

func tribonacci(n int) int {
	switch {
	case n == 0:
		return 0
	case n == 2 || n == 1:
		return 1
	case n == 3:
		return 2
	}
	T1 := 1
	T2 := 1
	T3 := 2
	n -= 3
	for n != 0{
		sum := T1 + T2 + T3
		T1 = T2
		T2 = T3
		T3 = sum
		n--
	}
	return T3

}

