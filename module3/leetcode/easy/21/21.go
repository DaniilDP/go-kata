package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	fmt.Println(subtractProductAndSum(234)) // 15
}

func subtractProductAndSum(n int) int {
	sum := 0
	mul := 1
	for _, v := range strings.Split(strconv.Itoa(n), "") {
		vint, _ := strconv.Atoi(v)
		sum += vint
		mul *= vint
	}
	return mul - sum
}