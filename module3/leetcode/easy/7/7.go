package main

import (
	"strings"
	"fmt"
)

func main() {
	fmt.Println(defangIPaddr("255.100.50.0"))
}

func defangIPaddr(address string) string {
	return strings.Replace(address, ".", "[.]",-1)
}