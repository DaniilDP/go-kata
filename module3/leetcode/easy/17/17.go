package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(mostWordsFound([]string{"please wait", "continue to fight", "continue to win"})) // 3
}

func mostWordsFound(sentences []string) int {
	result := 0
	for _, v := range sentences {
		lenv := len(strings.Fields(v))
		if lenv > result {
			result = lenv
		}
	}
	return result
}