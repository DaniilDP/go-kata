package pkg_strategy

type Strategy interface {
	Route(startPoint int, endPoint int)
}