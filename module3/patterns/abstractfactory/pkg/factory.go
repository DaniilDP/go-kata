package pkgfactory

import (
	"fmt"
)

type Factory interface {
	GetComputer() Computer
	GetMonitor() Monitor
}

func GetFactory(brand string) (Factory, error) {
	switch brand {
	default:
		return nil, fmt.Errorf("производитель %s - не найден", brand)
	case Asus:
		return &AsusFactory{}, nil
	case HP:
		return &HpFactory{}, nil
	}
}