package main

import (
	pkg "gitlab.com/DaniilDP/go-kata/module3/patterns/abstractfactory/pkg"
)

var	(
	brands = []string{pkg.Asus, pkg.HP, "dell"}
)

func main() {
	for _, brand := range brands {
		factory, err := pkg.GetFactory(brand)
		if err != nil {
			println(err.Error())
			continue
		}
		monitor := factory.GetMonitor()
		monitor.PrintDetails()
		computer := factory.GetComputer()
		computer.PrintDetails()
	}
}