package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"github.com/brianvoe/gofakeit/v6"
	"math/rand"
	"os"
	"time"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	Pop() *Node
	Shift() *Node
	Reverse() *DoubleLinkedList
	Search(message string) *Node
	SearchUUID(uuID string) *Node
	LoadData(path string) error
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	fileJSON, err := os.Open(path)

	if err != nil {
		return errors.New("file not found")
	}

	data, _ := bufio.NewReader(fileJSON).ReadBytes(byte('\n'))

	var commit []Commit
	err = json.Unmarshal(data, &commit)
	if err != nil {
		return err
	}
	commit = QuickSort(commit)

	for _, c := range commit {
		err = d.Insert(d.len, c)
		if err != nil {
			return err
		}
	}
	return nil
}

// QuickSort сортировка среза
func QuickSort(arr []Commit) []Commit {

	if len(arr) <= 1 {
		return arr
	}

	median := arr[rand.Intn(len(arr))].Date

	low_part := make([]Commit, 0, len(arr))
	high_part := make([]Commit, 0, len(arr))
	middle_part := make([]Commit, 0, len(arr))

	for _, item := range arr {
		switch {
		case item.Date.Before(median):
			low_part = append(low_part, item)
		case item.Date.Equal(median):
			middle_part = append(middle_part, item)
		case item.Date.After(median):
			high_part = append(high_part, item)
		}
	}

	low_part = QuickSort(low_part)
	high_part = QuickSort(high_part)

	low_part = append(low_part, middle_part...)
	low_part = append(low_part, high_part...)

	return low_part
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr != nil {
		d.curr = d.curr.next
	}
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr != nil {
		d.curr = d.curr.prev
	}
	return d.curr
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if d.len == 0 {
		newNode := &Node{data: &c}
		d.head = newNode
		d.tail = newNode
		d.curr = newNode
		d.len++
		return nil
	}
	if n == d.len {
		newNode := &Node{data: &c, prev: d.tail}
		d.tail.next = newNode
		d.tail = newNode
		d.curr = newNode
		d.len++
		return nil
	}
	if n == 0 {
		newNode := &Node{data: &c, next: d.head}
		d.head.prev = newNode
		d.head = newNode
		d.curr = newNode
		d.len++
		return nil
	}
	if n > 0 && n < d.len {
		newNode := &Node{data: &c, prev: d.curr, next: d.curr.next}
		d.curr.next.prev = newNode
		d.curr.next = newNode
		d.curr = newNode
		d.len++
		return nil
	}
	return errors.New("index out of range")
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if d.len == 0 {
		return errors.New("list is empty")
	}

	if n < 1 || n > d.len {
		return errors.New("index out of range")
	}

	if n == 1 {
		d.head = d.head.next
		if d.head != nil {
			d.head.prev = nil
		} else {
			d.tail = nil
		}
	} else if n == d.len {
		d.tail = d.tail.prev
		if d.tail != nil {
			d.tail.next = nil
		} else {
			d.head = nil
		}
	} else {
		node := d.head
		for i := 1; i < n; i++ {
			node = node.next
		}
		node.prev.next = node.next
		node.next.prev = node.prev
	}

	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	indexCurrent, err := d.Index()

	if err != nil {
		return err
	}

	err = d.Delete(indexCurrent)

	if err != nil {
		return err
	}

	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	//huita ispravitь
	if d.len == 0 {
		return 0, errors.New("list is empty")
	}

	if d.curr == nil {
		return 0, errors.New("current element is nil")
	}

	index := 1
	node := d.head
	for node != nil && node != d.curr {
		node = node.next
		index++
	}

	if node == nil {
		return 0, errors.New("current element not found")
	}

	return index, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	return d.head
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	return d.tail
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {

	if d.len == 0 || d.curr == nil {
		return nil
	}

	node := d.head
	for node != nil {
		if node.data.UUID == uuID {
			return node
		}
		node = node.next
	}

	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	if d.len == 0 || d.curr == nil {
		return nil
	}

	node := d.head
	for node != nil {
		if node.data.Message == message {
			return node
		}
		node = node.next
	}

	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	node := d.head
	for node != nil {
		prev := node.prev
		node.prev = node.next
		node.next = prev
		node = node.prev
	}

	d.head, d.tail = d.tail, d.head
	return d
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON(n int, filename string) {
	commits := make([]Commit, n)
	for i := 0; i < n; i++ {
		commits[i] = Commit{
			Message: gofakeit.Sentence(6),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.DateRange(time.Date(1900, 1, 1, 0, 0, 0, 0, time.UTC), time.Now()).UTC(),
		}
	}

	jsonData, _ := json.MarshalIndent(commits, "", "    ")

	_ = os.WriteFile(filename, jsonData, 0644)
}

func main() {
	GenerateJSON(20, "module3/task/test2.json")
}
