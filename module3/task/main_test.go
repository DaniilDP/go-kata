package main

import (
	"fmt"
	"testing"
	"time"
)

func TestDoubleLinkedList(t *testing.T) {
	fields := DoubleLinkedList{}
	err := fields.LoadData("./test.json")
	if err != nil {
		panic("Файл не найден")
	}
	timelastcommit, _ := time.Parse("2006-01-02 15:04:05.999999999", "2022-10-03 10:24:08.872452827")
	timefirstcommit, _ := time.Parse("2006-01-02 15:04:05.999999999", "1899-12-09 04:13:38.386979551")

	type args struct {
		data string
	}

	tests := []struct {
		name   string
		fields DoubleLinkedList
		args
		want interface{}
	}{
		{
			name:   "len",
			fields: fields,
			want:   2000,
		},
		{
			name:   "current",
			fields: fields,
			want:   fields.curr,
		},
		{
			name:   "next",
			fields: fields,
			want:   fields.curr.next,
		},
		{
			name:   "prev",
			fields: fields,
			want:   fields.curr.prev,
		},
		{
			name:   "pop",
			fields: fields,
			want:   fields.head,
		},
		{
			name:   "shift",
			fields: fields,
			want:   fields.tail,
		},
		{
			name:   "reverse",
			fields: fields,
			want:   timelastcommit,
		},
		{
			name:   "search",
			fields: fields,
			args: args{
				data: "You can't generate the array without programming the auxiliary USB monitor!",
			},
			want: Commit{
				`You can't generate the array without programming the auxiliary USB monitor!`,
				`6958701a-875b-11ed-8150-acde48001122`,
				timefirstcommit,
			},
		},
		{
			name:   "searchUUID",
			fields: fields,
			args: args{
				data: `6958701a-875b-11ed-8150-acde48001122`,
			},
			want: Commit{
				`You can't generate the array without programming the auxiliary USB monitor!`,
				`6958701a-875b-11ed-8150-acde48001122`,
				timefirstcommit,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			switch {
			case tt.name == "len":
				if got := d.Len(); got != tt.want {
					t.Errorf("Len() = %v, want %v", got, tt.want)
				}
			case tt.name == "current":
				if got := d.Current(); got != tt.want {
					t.Errorf("Current() = %v, want %v", got, tt.want)
				}
			case tt.name == "next":
				if got := d.Next(); got != tt.want {
					t.Errorf("Next() = %v, want %v", got, tt.want)
				}
			case tt.name == "prev":
				if got := d.Prev(); got != tt.want {
					t.Errorf("Prev() = %v, want %v", got, tt.want)
				}
			case tt.name == "pop":
				if got := d.Pop(); got != tt.want {
					t.Errorf("Pop() = %v, want %v", got, tt.want)
				}
			case tt.name == "shift":
				if got := d.Shift(); got != tt.want {
					t.Errorf("Shift() = %v, want %v", got, tt.want)
				}
			case tt.name == "reverse":
				got := d.Reverse().head.data.Date
				if got != tt.want {
					t.Errorf("Reverse() = %v, want %v", got, tt.want)
				}
			case tt.name == "search":
				if got := *d.Search(tt.args.data).data; got != tt.want {
					t.Errorf("Search() = %v, want %v", got, tt.want)
				}
			case tt.name == "searchUUID":
				if got := *d.SearchUUID(tt.args.data).data; got != tt.want {
					t.Errorf("SearchUUID() = %v, want %v", got, tt.want)
				}
			}

		})
	}
}

func BenchmarkDoubleLinkedList(b *testing.B) {
	fields := DoubleLinkedList{}
	b.ResetTimer()
	err := fields.LoadData("./test.json")
	if err != nil {
		fmt.Println("Файл не найден")
	}
}
