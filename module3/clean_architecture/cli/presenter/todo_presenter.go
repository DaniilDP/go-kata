package presenter

import (
	"fmt"

	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/service"
)

type todoPresenter struct {
	service service.TodoService
}

type TodoPresenter interface {
	ListTodos()
	AddTodo(title string)
	DeleteTodo(id int)
	DoneTodo(id int)
	PatchTodo(id int, title, description string, isDone bool)
}

func NewPresentor(service service.TodoService) *todoPresenter {
	return &todoPresenter{service: service}
}

func (p todoPresenter) ListTodos() {
	todos, err := p.service.ListTodos()
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, todo := range todos {
		fmt.Printf("[%d] %s - %t \n", todo.ID, todo.Title, todo.IsDone)
	}
}

func (p todoPresenter) AddTodo(title string) {
	if err := p.service.CreateTodo(title); err != nil {
		fmt.Println(err)
	}
}

func (p todoPresenter) DeleteTodo(id int) {
	todo, err := p.service.FindTodo(id)
	if err != nil {
		fmt.Println(err)
	}
	if err := p.service.RemoveTodo(todo); err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Todo with id [%d] is removed\n", todo.ID)
}

func (p todoPresenter) DoneTodo(id int) {
	todo, err := p.service.FindTodo(id)
	if err != nil {
		fmt.Println(err)
	}
	if err := p.service.CompleteTodo(todo); err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Todo with id [%d] is done\n", todo.ID)
}

func (p todoPresenter) PatchTodo(id int, title, description string, isDone bool) {
	todo, err := p.service.FindTodo(id)
	if err != nil {
		fmt.Println(err)
		return
	}
	todo.Title = title
	todo.Description = description
	todo.IsDone = isDone
	if err := p.service.PatchTodo(todo); err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Todo with id [%d] is patched\n", todo.ID)
}
