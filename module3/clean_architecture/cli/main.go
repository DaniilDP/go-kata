package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/cli/presenter"
	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/service"
)

func main() {
	repo := repo.FileTaskRepository{FilePath: "./data.json"}
	service := service.NewService(&repo)
	presenter := presenter.NewPresentor(service)

	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Println("Enter Commands to use todo_cli\n",
			"A - Show all todos\n",
			"B - Add todo\n",
			"C - Delete todo\n",
			"D - Patch todo\n",
			"E - Done todo\n",
			"F - exit")

		switch command, _ := reader.ReadString('\n'); command[0] {
		case 'A':
			presenter.ListTodos()
		case 'B':
			fmt.Println("Enter title for todo")
			title, _ := reader.ReadString('\n')
			presenter.AddTodo(title)
		case 'C':
			fmt.Println("Enter the id of todo")
			s_id, _ := reader.ReadString('\n')
			id, _ := strconv.Atoi(s_id)
			presenter.DeleteTodo(id)
		case 'D':
			fmt.Println("Enter the id of todo")
			s_id, _ := reader.ReadString('\n')
			id, _ := strconv.Atoi(s_id)
			fmt.Println("Enter the title of todo")
			title, _ := reader.ReadString('\n')
			fmt.Println("Enter the description of todo")
			description, _ := reader.ReadString('\n')
			fmt.Println("Enter 1 if todo is done (0 overwise)")
			s_isDone, _ := reader.ReadString('\n')
			is, _ := strconv.Atoi(s_isDone)
			isDone := false
			if is == 1 {
				isDone = true
			}
			presenter.PatchTodo(id, title, description, isDone)
		case 'E':
			fmt.Println("Enter the id of todo")
			s_id, _ := reader.ReadString('\n')
			id, _ := strconv.Atoi(s_id)
			presenter.DoneTodo(id)
		case 'F':
			break
		default:
			fmt.Println("Enter the correct command!")
		}
	}
}
