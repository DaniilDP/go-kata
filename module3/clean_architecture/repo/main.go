package main

import (
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	repository "gitlab.com/DaniilDP/go-kata/module3/clean_architecture/repo/repository"
	"os"
)

func main() {
	file, err := os.OpenFile("module3/clean_architecture/repo/test.json", os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	repo := repository.NewUserRepository(file)

	// добавление пользователей
	for i := 0; i < 5; i++ {
		newUser := repository.User{
			ID:   i,
			Name: fmt.Sprintf("%s %s", gofakeit.FirstName(), gofakeit.LastName()),
		}

		err = repo.Save(newUser)
		if err != nil {
			panic(err)
		}
	}

	// поиск по id
	fmt.Println(repo.Find(1))

	// показать всё
	fmt.Println(repo.FindAll())
}
