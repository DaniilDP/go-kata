package repository

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriteSeeker
}

func NewUserRepository(file io.ReadWriteSeeker) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

//Загружать данные в конструкторе
func (r *UserRepository) Save(record interface{}) error {
	var alluser []User
	var buf bytes.Buffer

	if _, err := r.File.Seek(0, io.SeekStart); err != nil {
		return err
	}

	_, _ = io.Copy(&buf, r.File)
	_ = json.Unmarshal(buf.Bytes(), &alluser)

	switch v := record.(type) {
	case User:
		alluser = append(alluser, v)
	case []User:
		alluser = append(alluser, v...)
	default:
		return errors.New("invalid record type")
	}

	data, err := json.MarshalIndent(alluser, "", "    ")
	if err != nil {
		return err
	}

	if _, err := r.File.Seek(0, io.SeekStart); err != nil {
		return err
	}

	_, err = r.File.Write(data)
	if err != nil {
		return err
	}

	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	var alluser []User
	var buf bytes.Buffer

	if _, err := r.File.Seek(0, io.SeekStart); err != nil {
		return nil, err
	}

	_, _ = io.Copy(&buf, r.File)
	_ = json.Unmarshal(buf.Bytes(), &alluser)

	for _, user := range alluser {
		if user.ID == id {
			return user, nil
		}
	}

	return nil, errors.New("id not found")
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	var alluser []User
	var buf bytes.Buffer

	if _, err := r.File.Seek(0, io.SeekStart); err != nil {
		return nil, err
	}

	_, _ = io.Copy(&buf, r.File)
	_ = json.Unmarshal(buf.Bytes(), &alluser)

	if len(alluser) > 0 {
		result := make([]interface{}, len(alluser))
		for i, v := range alluser {
			result[i] = v
		}
		return result, nil
	} else {
		return nil, errors.New("file is empty")
	}
}
