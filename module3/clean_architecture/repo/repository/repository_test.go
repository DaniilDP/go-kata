package repository

import (
	"os"
	"reflect"
	"testing"
)

func TestUserRepository_FindAll(t *testing.T) {

	file, err := os.OpenFile("./test.json", os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	fields := NewUserRepository(file)

	tests := []struct {
		name    string
		fields  *UserRepository
		want    []User
		wantErr bool
	}{
		{
			name:   "findall",
			fields: fields,
			want: []User{{0, "Jovany Mitchell"},
				{1, "Sylvester Jones"},
				{2, "Franz Keeling"},
				{3, "Lauren Spencer"},
				{4, "Zula Kreiger"}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.FindAll()

			users := make([]User, len(got))
			for i, v := range got {
				users[i] = v.(User)
			}

			if err != nil {
				t.Errorf("Findall() error = %v, want nil", err)
			}
			if !reflect.DeepEqual(users, tt.want) {
				t.Errorf("Findall() = %v, want %v", got, tt.want)
			}

		})
	}
}

func TestUserRepository_Find(t *testing.T) {

	file, err := os.OpenFile("./test.json", os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	fields := NewUserRepository(file)

	tests := []struct {
		name     string
		fields   *UserRepository
		searchid int
		want     User
		wantErr  bool
	}{
		{
			name:     "find",
			fields:   fields,
			searchid: 3,
			want:     User{3, "Lauren Spencer"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.Find(tt.searchid)
			if err != nil {
				t.Errorf("Find() error = %v, want nil", err)
			}
			if got != tt.want {
				t.Errorf("Find() = %v, want %v", got, tt.want)
			}

		})
	}

}
