package main

import (
	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/service"
)

func main() {
	repo := repo.FileTaskRepository{FilePath: "./test.json"}
	service := service.NewService(&repo)

	_ = service.CreateTodo("first")

	_ = service.CompleteTodo(model.Todo{ID: 1, Title: "Complete first"})

}