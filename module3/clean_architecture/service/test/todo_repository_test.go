package tests

import (
	"reflect"
	"testing"

	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/repo"
)

func TestRepository_GetTasks(t *testing.T) {
	type fields struct {
		FilePath string
	}
	tests := []struct {
		name    string
		fields  fields
		want    []repo.Task
		wantErr bool
	}{
		{name: "test",
			fields: fields{FilePath: "./data_repo.json"},
			want: []repo.Task{
				{ID: 1, Title: "1", Description: "68", IsDone: false},
				{ID: 2, Title: "2", Description: "69", IsDone: false},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.GetTasks()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTasks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTasks() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRepository_SaveTasks(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		tasks []repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "save",
			fields: fields{FilePath: "./data_repo.json"},
			args: args{[]repo.Task{{ID: 1, Title: "1", IsDone: false, Description: "68"},
				{ID: 2, Title: "2", IsDone: false, Description: "69"}}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			if err := repo.SaveTasks(tt.args.tasks); (err != nil) != tt.wantErr {
				t.Errorf("SaveTasks() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestRepository_CreateTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		task repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    repo.Task
		wantErr bool
	}{
		{name: "save",
			fields:  fields{FilePath: "./data_repo.json"},
			args:    args{repo.Task{ID: 3, Title: "created", IsDone: false, Description: ""}},
			want:    repo.Task{ID:3, Title: "created", IsDone: false, Description: ""},
			wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.CreateTask(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateTask() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRepository_UpdateTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		task repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    repo.Task
		wantErr bool
	}{
		{
			name:    "update",
			fields:  fields{FilePath: "./data_repo.json"},
			args:    args{task: repo.Task{ID: 3, Title: "11", Description: "1", IsDone: true}},
			want:    repo.Task{ID: 3, Title: "11", Description: "1", IsDone: true},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.UpdateTask(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateTask() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRepository_DeleteTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "deleting",
			fields:  fields{FilePath: "./data_repo.json"},
			args:    args{id: 3},
			wantErr: false,
		},
		{
			name:    "err deleting",
			fields:  fields{FilePath: "./data_repo.json"},
			args:    args{id: 69},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			
			if err := repo.DeleteTask(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("DeleteTask() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

