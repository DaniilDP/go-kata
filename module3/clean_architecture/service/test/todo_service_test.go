package tests

import (
	"reflect"
	"testing"

	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/service"
)

func Test_todoService_ListTodos(t *testing.T) {
	type fields struct {
		repo repo.TaskRepository
	}
	tests := []struct {
		name    string
		fields  fields
		want    []model.Todo
		wantErr bool
	}{
		{
			name:   "list",
			fields: fields{repo: repo.NewRepository("./service_list_todo.json")},
			want: []model.Todo{
				{
					ID:          1,
					IsDone:      false,
					Title:       "first",
					Description: "description 1",
				},
				{
					ID:          2,
					IsDone:      true,
					Title:       "second",
					Description: "description 2"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)
			got, err := s.ListTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("ListTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListTodos() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_todoService_CreateTodo(t *testing.T) {
	type fields struct {
		repo repo.TaskRepository
	}
	type args struct {
		title string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{name: "creating",
			fields:  fields{repo: repo.NewRepository("./service_createtodo.json")},
			args:    args{title: "service_CreateTodo"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)
			if err := s.CreateTodo(tt.args.title); (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_CompleteTodo(t *testing.T) {
	type fields struct {
		repo repo.TaskRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "compete",
			fields:  fields{repo: repo.NewRepository("./service_completetodo.json")},
			args:    args{model.Todo{ID: 1, Title: "first", Description: "", IsDone: false}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)
			if err := s.CompleteTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("CompleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_RemoveTodo(t *testing.T) {
	type fields struct {
		repo repo.TaskRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "deleting",
			fields: fields{repo: repo.NewRepository("./service_removeTodo.json")},
			args: args{
				todo: model.Todo{
					ID:          1,
					IsDone:      true,
					Title:       "first",
					Description: "",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)
			if err := s.RemoveTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("RemoveTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
