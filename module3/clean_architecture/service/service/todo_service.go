package service

import (
	"fmt"

	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/DaniilDP/go-kata/module3/clean_architecture/service/repo"
)

type TodoService interface {
	ListTodos() ([]model.Todo, error)
	CreateTodo(title string) error
	FindTodo(id int) (model.Todo, error)
	CompleteTodo(todo model.Todo) error
	RemoveTodo(todo model.Todo) error
	PatchTodo(todo model.Todo) error
}

type todoService struct {
	repo repo.TaskRepository
}

func NewService(repo repo.TaskRepository) *todoService {
	return &todoService{
		repo: repo,
	}
}

func (s *todoService) ListTodos() ([]model.Todo, error) {
	tasks, err := s.repo.GetTasks()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	todos := make([]model.Todo, 0, len(tasks))
	for _, task := range tasks {
		todos = append(todos, model.Todo{ID: task.ID, Title: task.Title, Description: task.Description, IsDone: task.IsDone})
	}
	return todos, nil
}

func (s *todoService) FindTodo(id int) (model.Todo, error) {
	todos, err := s.ListTodos()
	if err != nil {
		return model.Todo{}, err
	}

	for _, todo := range todos {
		if todo.ID == id {
			return todo, nil
		}
	}
	return model.Todo{}, fmt.Errorf("todo.id not found")
}

func (s *todoService) CreateTodo(title string) error {
	task := repo.Task{Title: title}
	_, err := s.repo.CreateTask(task)
	if err != nil {
		return err
	}
	return nil
}

func (s *todoService) CompleteTodo(todo model.Todo) error {
	task := repo.Task{ID: todo.ID, Title: todo.Title, Description: todo.Description, IsDone: !todo.IsDone}
	_, err := s.repo.UpdateTask(task)
	if err != nil {
		return err
	}
	return nil
}

func (s *todoService) RemoveTodo(todo model.Todo) error {
	err := s.repo.DeleteTask(todo.ID)
	if err != nil {
		return err
	}
	return nil
}

func (s *todoService) PatchTodo(todo model.Todo) error {
	task := repo.Task{ID: todo.ID, Title: todo.Title, Description: todo.Description, IsDone: !todo.IsDone}
	if _, err := s.repo.UpdateTask(task); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}
