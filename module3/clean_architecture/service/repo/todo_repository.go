package repo

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
)


type Task struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	IsDone      bool   `json:"status"`
}

type TaskRepository interface {
	GetTasks() ([]Task, error)
	GetTask(id int) (Task, error)
	CreateTask(task Task) (Task, error)
	UpdateTask(task Task) (Task, error)
	DeleteTask(id int) error
	SaveTasks(tasks []Task) error
}

type FileTaskRepository struct {
	FilePath string
}

func NewRepository(filepath string) *FileTaskRepository {
	return &FileTaskRepository{FilePath: filepath}
}

func (repo *FileTaskRepository) DeleteTask(id int) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	for _, task := range tasks {
		if task.ID == id {
			if len(tasks) != task.ID {
				tasks = append(tasks[:task.ID], tasks[task.ID+1:]...)
			} else {
				tasks = append(tasks[:task.ID], tasks[task.ID:]...)
			}
			_ = tasks
			return nil
		}
	}
	return fmt.Errorf("no task.id %d", id)
}

func (repo *FileTaskRepository) GetTasks() ([]Task, error) {
	var tasks []Task

	file, err := os.OpenFile(repo.FilePath, os.O_RDWR|os.O_CREATE, 0755)

	if err != nil {
		return nil, err
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(content, &tasks); err != nil {
		return nil, err
	}

	return tasks, nil
}

func (repo *FileTaskRepository) GetTask(id int) (Task, error) {
	var task Task

	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for _, t := range tasks {
		if t.ID == id {
			return t, nil
		}
	}

	return task, fmt.Errorf("no task.id: %d ", id)
}

func (repo *FileTaskRepository) SaveTasks(tasks []Task) error {
	data, err := json.MarshalIndent(tasks, "", "	")
	if err != nil {
		return err
	}
	f, err := os.OpenFile(repo.FilePath, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		return err
	}
	if _, err = f.Seek(0, 0); err != nil {
		return err
	}
	defer f.Close()
	if _, err = f.Write(data); err != nil {
		return err
	}
	return nil
}

func (repo *FileTaskRepository) CreateTask(task Task) (Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}
	task.ID = len(tasks) + 1
	tasks = append(tasks, task)
	if err := repo.SaveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}

func (repo *FileTaskRepository) UpdateTask(task Task) (Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}
	i := 0
	for _, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
		i++
	}
	if i >= len(tasks) {
		return Task{}, fmt.Errorf("no todo.id %d", task.ID)
	}
	if err := repo.SaveTasks(tasks); err != nil {
		return Task{}, err
	}

	return task, nil
}
