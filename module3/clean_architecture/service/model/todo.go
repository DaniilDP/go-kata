package model

type Todo struct {
	ID          int
	Title       string
	IsDone      bool
	Description string
}