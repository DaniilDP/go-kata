package algo

import (
	"math/rand"
	"time"
)

func QuickSort(arr []int) []int {

	if len(arr) <= 1 {
		return arr
	}

	median := arr[rand.Intn(len(arr))]

	low_part := make([]int, 0, len(arr))
	high_part := make([]int, 0, len(arr))
	middle_part := make([]int, 0, len(arr))

	for _, item := range arr {
		switch {
		case item < median:
			low_part = append(low_part, item)
		case item == median:
			middle_part = append(middle_part, item)
		case item > median:
			high_part = append(high_part, item)
		}
	}

	low_part = QuickSort(low_part)
	high_part = QuickSort(high_part)

	low_part = append(low_part, middle_part...)
	low_part = append(low_part, high_part...)

	return low_part
}

func randomData(n, max int) func() []int {
	var data []int
	return func() []int {
		if data != nil {
			return data
		}

		rand.Seed(time.Now().UnixNano())

		for i := 0; i < n; i++ {
			data = append(data, rand.Intn(max))
		}

		return data
	}
}
