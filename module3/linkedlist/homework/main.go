package main

import (
	"errors"
	"fmt"
	"time"
)

type Post struct {
	body        string //текст поста
	publishDate int64  //дата публикации поста в формате Unix timestamp
	next        *Post  //указатель на следующий пост в потоке
}

type Feed struct {
	length int   //количество постов в потоке
	start  *Post //указатель на первый пост в потоке.
	end    *Post //указатель на последний пост в потоке
}

func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		lastPost := f.end
		lastPost.next = newPost
		f.end = newPost
	}
	f.length++
}

func (f *Feed) Remove(publishDate int64) {
	if f.length == 0 {
		panic(errors.New("Feed is empty"))
	}

	if f.start.publishDate == publishDate {
		f.start = f.start.next
	}

	var previousPost *Post
	currentPost := f.start

	for currentPost.publishDate != publishDate {
		if currentPost.next == nil {
			panic(errors.New("No such Post found."))
		}

		previousPost = currentPost
		currentPost = currentPost.next
	}
	previousPost.next = currentPost.next

	f.length--
}

func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else if newPost.publishDate > f.start.publishDate {
		newPost.next = f.start
		f.start = newPost
	} else {
		var previousPost *Post
		currentPost := f.start

		for currentPost.publishDate < newPost.publishDate {
			previousPost = currentPost
			currentPost = previousPost.next
		}

		previousPost.next = newPost
		newPost.next = currentPost
	}

	f.length++
}

func (f *Feed) Inspect() {
	if f.length == 0 {
		fmt.Println("Feed is empty")
	}
	fmt.Println("========================")
	fmt.Println("Feed Length: ", f.length)

	currentIndex := 0
	currentPost := f.start

	for currentIndex < f.length {
		fmt.Printf("Item: %v - %v\n", currentIndex, currentPost)
		if currentPost.next == nil {
			break
		}
		currentPost = currentPost.next
		currentIndex++
	}
	fmt.Println("========================")
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)

	f.Inspect()

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 15,
	}
	f.Insert(newPost)
	f.Inspect()
}
